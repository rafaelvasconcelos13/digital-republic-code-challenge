import React from "react";
import Formulario from "./Formulario";

function App() {
  const [numeroDeLatas, setNumeroDeLatas] = React.useState("");
  const [dados, setDados] = React.useState({
    altura1: undefined,
    largura1: undefined,
    numeroDePortas1: undefined,
    numeroDeJanelas1: undefined,
    altura2: undefined,
    largura2: undefined,
    numeroDePortas2: undefined,
    numeroDeJanelas2: undefined,
    altura3: undefined,
    largura3: undefined,
    numeroDePortas3: undefined,
    numeroDeJanelas3: undefined,
    altura4: undefined,
    largura4: undefined,
    numeroDePortas4: undefined,
    numeroDeJanelas4: undefined,
  });

  function handleChange(event, index) {
    setDados((prevState) => {
      return {
        ...prevState,
        [event.target.id + index]: event.target.value.replace(",", "."),
      };
    });
  }

  // const numb = num / 5;
  // 44,32 - 8,86
  // 3,6 3,6 0,5 0,5 0,5 0,5
  function calculadora(num) {
    const arr = [
      [18, "18l "],
      [3.6, "3,6l "],
      [2.5, "2,5l "],
      [0.5, "0,5l "],
    ];
    if (num < 0) {
      return "";
    }
    for (let i = 0; i < arr.length; i++) {
      if (num >= arr[i][0]) {
        return arr[i][1] + calculadora(num - arr[i][0]);
      } else if (num < 0.5) {
        return arr[3][1];
      }
    }
  }

  function fraseador(num) {
    const dezoito = num.split(" ").filter((i) => i === "18l").length;
    const tres = num.split(" ").filter((i) => i === "3,6l").length;
    const dois = num.split(" ").filter((i) => i === "2,5l").length;
    const meio = num.split(" ").filter((i) => i === "0,5l").length;
    return (
      "Você precisará de " +
      (dezoito > 0 ? dezoito + " lata(s) de 18 litros, " : "") +
      (tres > 0 ? tres + " lata(s) de 3,6 litros, " : "") +
      (dois > 0 ? dois + " lata(s) de 2,5 litros, " : "") +
      (meio > 0 ? meio + " lata(s) de 0,5 litros, " : "")
    );
  }

  function handleSubmit(event) {
    event.preventDefault();
    const janela = 2.4;
    const porta = 1.52;
    const areaPortasJanelas =
      porta * dados.numeroDePortas1 +
      porta * dados.numeroDePortas2 +
      porta * dados.numeroDePortas3 +
      porta * dados.numeroDePortas4 +
      janela * dados.numeroDeJanelas1 +
      janela * dados.numeroDeJanelas2 +
      janela * dados.numeroDeJanelas3 +
      janela * dados.numeroDeJanelas4;

    const areaParedes =
      dados.altura1 * dados.largura1 +
      dados.altura2 * dados.largura2 +
      dados.altura3 * dados.largura3 +
      dados.altura4 * dados.largura4;
    const areaLiquida = areaParedes - areaPortasJanelas;
    if (
      dados.altura1 * dados.largura1 < 1 ||
      dados.altura1 * dados.largura1 > 15 ||
      dados.altura2 * dados.largura2 < 1 ||
      dados.altura2 * dados.largura2 > 15 ||
      dados.altura3 * dados.largura3 < 1 ||
      dados.altura3 * dados.largura3 > 15 ||
      dados.altura4 * dados.largura4 < 1 ||
      dados.altura4 * dados.largura4 > 15
    ) {
      setNumeroDeLatas(`* Nenhuma parede pode ter menos de 1 metro quadrado nem mais de 15 metros quadrados. 
      Preencha novamente com novas dimensões.`);
    } else if (areaPortasJanelas > areaParedes / 2) {
      setNumeroDeLatas(
        `* O total de área das portas e janelas deve ser no máximo 50% da área de parede.`
      );
    } else if (
      (dados.altura1 < 1.9 + 0.3 && dados.numeroDePortas1 > 0) ||
      (dados.altura2 < 1.9 + 0.3 && dados.numeroDePortas2 > 0) ||
      (dados.altura3 < 1.9 + 0.3 && dados.numeroDePortas3 > 0) ||
      (dados.altura4 < 1.9 + 0.3 && dados.numeroDePortas4 > 0)
    ) {
      setNumeroDeLatas(`* A altura de paredes com porta deve ser, no mínimo, 
      30 centímetros maior que a altura da porta.`);
    } else if (areaParedes > 0) {
      return setNumeroDeLatas(
        fraseador(calculadora(areaLiquida / 5)).replace(/,\s$/, ".")
      );
    } else {
      setNumeroDeLatas(undefined);
    }
  }

  const resposta = /^\*/.test(numeroDeLatas)
    ? {
        alignSelf: "center",
        marginTop: "1.5rem",
        width: "30rem",
        color: "rgba(255, 0, 0, 0.897)",
        fontSize: "0.8rem",
        display: "flex",
        justifyContent: "center",
      }
    : {
        alignSelf: "center",
        display: "flex",
        justifyContent: "center",
        marginTop: "2.5rem",
        width: "50rem",
        color: "rgba(255, 255, 255, 0.945)",
        fontSize: "1.5rem",
        height: "10rem",
      };

  return (
    <div className="principal">
      <h1>Calculadora de Tinta</h1>
      <h2>Por favor, informe as dimensões de cada parede</h2>
      <form className="dados" id="my-form">
        {new Array(4).fill(1).map((_, index) => (
          <Formulario
            index={index}
            key={index}
            dados={dados}
            handleChange={handleChange}
          />
        ))}
      </form>
      <button type="submit" form="my-form" onClick={handleSubmit}>
        Enviar
      </button>
      {/* numeroDeLatas &&  */ <p style={resposta}>{numeroDeLatas}</p>}
    </div>
  );
}

export default App;
