// eslint-disable-next-line import/no-anonymous-default-export
export default function (props) {
  return (
    <div className="parede">
      <h3>Parede {props.index + 1}:</h3>
      <input
        id="altura"
        placeholder="Altura"
        value={props.dados[`altura${props.index + 1}`]}
        onChange={(event) => props.handleChange(event, props.index + 1)}
      ></input>
      <input
        id="largura"
        placeholder="Largura"
        value={props.dados[`largura${props.index + 1}`]}
        onChange={(event) => props.handleChange(event, props.index + 1)}
      ></input>
      <input
        id="numeroDePortas"
        placeholder="Quantidade de Portas"
        value={props.dados[`numeroDePortas${props.index + 1}`]}
        onChange={(event) => props.handleChange(event, props.index + 1)}
      ></input>
      <input
        id="numeroDeJanelas"
        placeholder="Quantidade de Janelas"
        value={props.dados[`numeroDeJanelas${props.index + 1}`]}
        onChange={(event) => props.handleChange(event, props.index + 1)}
      ></input>
    </div>
  );
}
