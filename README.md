# Digital Republic Code Challenge



## Getting started

Este é um aplicativo web feito com create react app. Para rodá-lo, é preciso clonar/baixar todos os arquivos no seu pc.

Em seguida, é necessário usar o npm. Abra o terminal, entre na pasta onde copiou os arquivos e inicie um servidor local com os seguintes comandos:

```
cd code-challenge
npm start

```
